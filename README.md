# Project Base for Axon and Spring Boot

Import the project to the IDE of your choosing as a Maven project. 

Run application using `mvn spring-boot:run` or directly running Application class from your IDE. 

To view H2 database go to localhost:8080/h2-console


