package com.wbrandt.pokedexDemo.spring
/*

import com.wbrandt.pokedexDemo.spring.api.NewPokedexCommand
import com.wbrandt.pokedexDemo.spring.api.NewPokedexEvent
import com.wbrandt.pokedexDemo.spring.commands.Pokemon
import org.axonframework.test.aggregate.AggregateTestFixture
import org.axonframework.test.aggregate.FixtureConfiguration
import org.junit.Before
import org.junit.Test

class TestNewPokedexCommand {
    private lateinit var fixture: AggregateTestFixture<Pokemon>

    @Before
    fun setUp() {
        fixture = AggregateTestFixture(Pokemon::class.java)
    }

    @Test
    fun createPokedexTest() {
        val newPokdexCommand = NewPokedexCommand("1", "Test")
        val newPokedexEvent = NewPokedexEvent("1", "test", 0)

        fixture
                .givenNoPriorActivity()
                .`when`(newPokdexCommand)
                .expectEvents(newPokedexEvent)
    }
}
*/
