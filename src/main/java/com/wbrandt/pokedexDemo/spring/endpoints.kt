package com.wbrandt.pokedexDemo.spring

import com.wbrandt.pokedexDemo.spring.api.CapturePokemonCommand
import com.wbrandt.pokedexDemo.spring.api.NewPokedexCommand
import com.wbrandt.pokedexDemo.spring.api.UpdatePokedexNameCommand
import com.wbrandt.pokedexDemo.spring.commands.Pokedex
import org.springframework.web.bind.annotation.PostMapping
import java.util.*
import org.axonframework.commandhandling.gateway.CommandGateway
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.actuate.trace.http.HttpTrace
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.RestController


@RestController
class Endpoints {
    private val id = UUID.randomUUID().toString()
    @Autowired
    private lateinit var commandGateway: CommandGateway

    @PostMapping("/newPokedex")
    fun NewPokedex(@RequestParam(name = "name") name: String) {
        val id = UUID.randomUUID().toString()
        val name = name
        commandGateway.sendAndWait<Pokedex>(NewPokedexCommand(id, name))
    }

    @PostMapping("/updatePokedexName")
    fun UpdatePokedexName(@RequestParam(name = "id") id: String, @RequestParam(name = "name") name: String) {
        val id = id
        val name = name
        commandGateway.sendAndWait<Pokedex>(UpdatePokedexNameCommand(id, name))
    }

    @PostMapping("/capturePokemon")
    fun CapturePokemon() {
        val name = "test"
        commandGateway.sendAndWait<Pokedex>(CapturePokemonCommand(id, id))
    }

}