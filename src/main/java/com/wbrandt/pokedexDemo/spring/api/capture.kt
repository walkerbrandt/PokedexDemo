package com.wbrandt.pokedexDemo.spring.api

import org.axonframework.modelling.command.TargetAggregateIdentifier
import javax.persistence.Entity
import javax.persistence.Id

data class CapturePokemonCommand(@TargetAggregateIdentifier val pokedexId: String, val pokemonId: String)
data class CapturePokemonEvent(val pokedexId: String, val pokemonId: String)

@Entity
data class CapturedPokemon(@Id var id: String, var pokedexId: Int, var PokemonId: Int) {
    constructor() : this("", 0, 0)
}