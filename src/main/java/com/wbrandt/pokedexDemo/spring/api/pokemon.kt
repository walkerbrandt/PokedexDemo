package com.wbrandt.pokedexDemo.spring.api

import org.axonframework.modelling.command.TargetAggregateIdentifier
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import javax.persistence.Entity
import javax.persistence.Id

data class NewPokemonCommand(@TargetAggregateIdentifier val id: String, val pokemonName: String, val color: String, val weight: Double, val type: String)
data class NewPokemonEvent(val id: String, val pokemonName: String, val color: String, val weight: Double, val type: String)

@Entity
data class Pokemon(@Id var id: String, var pokemonName: String, val color: String, val weight: Double, val type: String) {
    constructor() : this("", "", "", 0.0, "")
}

@Repository
interface PokemonRepository : CrudRepository<Pokemon, Long>