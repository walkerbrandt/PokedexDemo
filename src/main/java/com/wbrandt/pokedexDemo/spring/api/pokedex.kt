package com.wbrandt.pokedexDemo.spring.api

import org.axonframework.modelling.command.TargetAggregateIdentifier
import javax.persistence.Entity
import javax.persistence.Id

data class NewPokedexCommand(@TargetAggregateIdentifier val id: String, val pokedexName: String)
data class UpdatePokedexNameCommand(@TargetAggregateIdentifier val id: String, val pokedexName: String)
data class UpdatePokedexSizeCommand(@TargetAggregateIdentifier val id: String, val pokedex_size: Int)

//events should be immutable and depict something that already happened
data class NewPokedexEvent(val id: String, val pokedexName: String, val pokedex_size: Int)
data class UpdatePokedexNameEvent(val id: String, val pokedexName: String)
data class UpdatePokedexSizeEvent(val id: String, val pokedex_size: Int)

@Entity
data class Pokedex(@Id var id: String, var pokedexName: String, var size: Int) {
    constructor() : this("", "", 0)
}
