package com.wbrandt.pokedexDemo.spring;

import com.wbrandt.pokedexDemo.spring.api.*;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.common.jpa.EntityManagerProvider;
import org.axonframework.common.jpa.SimpleEntityManagerProvider;
import org.axonframework.common.transaction.NoTransactionManager;
import org.axonframework.eventsourcing.eventstore.EmbeddedEventStore;
import org.axonframework.eventsourcing.eventstore.EventStorageEngine;
import org.axonframework.eventsourcing.eventstore.EventStore;
import org.axonframework.eventsourcing.eventstore.inmemory.InMemoryEventStorageEngine;
import org.axonframework.eventsourcing.eventstore.jpa.JpaEventStorageEngine;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.UUID;

/**
 * The entry point of the Spring Boot application.
 */
@SpringBootApplication(
        scanBasePackages = {
                "org.axonframework.eventsourcing.eventstore.jpa",
                "com.wbrandt"
        }
)
public class Application extends SpringBootServletInitializer {


    @Bean
    public EventStorageEngine eventStorageEngine() {
        return new InMemoryEventStorageEngine();
    }

    @Bean
    public EventStore eventStore(EventStorageEngine eventStorageEngine) {
        return EmbeddedEventStore.builder()
                .storageEngine(eventStorageEngine)
                .build();
    }

    @Bean
    public CommandLineRunner runner(CommandGateway commandGateway) {
        return (args) -> {

            String id = UUID.randomUUID().toString();
            //make new pokedex
            NewPokedexCommand cmd = new NewPokedexCommand(id, "test");
            commandGateway.sendAndWait(cmd);

            //make pokemon
            NewPokemonCommand newPokemon = new NewPokemonCommand(UUID.randomUUID().toString(), "Test Pokemon", "Blue", 120.3, "Water");
            commandGateway.sendAndWait(newPokemon);

            UpdatePokedexNameCommand updateName = new UpdatePokedexNameCommand(id,"test2");
            commandGateway.sendAndWait(updateName);

            //capture pokemon
            /*CapturePokemonCommand capturePokemon = new CapturePokemonCommand("1", "1");
            commandGateway.send(capturePokemon);*/

            //make new pokedex
/*            UpdatePokedexSizeCommand updateSize = new UpdatePokedexSizeCommand(id, 1);
            commandGateway.send(updateSize);*/
        };
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
