package com.wbrandt.pokedexDemo.spring.commands

import com.wbrandt.pokedexDemo.spring.api.*
import org.axonframework.commandhandling.CommandHandler
import org.axonframework.eventsourcing.EventSourcingHandler
import org.axonframework.modelling.command.AggregateIdentifier
import org.axonframework.spring.stereotype.Aggregate
import org.slf4j.LoggerFactory
import java.lang.invoke.MethodHandles
import java.util.*

import org.axonframework.modelling.command.AggregateLifecycle.apply
@Aggregate
class Pokemon {
    @AggregateIdentifier
    lateinit var id: String
    private var pokemonWeight: Double = 0.0
    private var pokemonName: String? = null
    private var captured: Boolean = false
    private var type: String? = null

    private val log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass())

    constructor() {
        log.debug("empty constructor invoked")
    }

    @CommandHandler
    constructor(cmd: NewPokemonCommand) {
        log.debug("check and see if the pokemon exists")
        apply(NewPokemonEvent(cmd.id, cmd.pokemonName, cmd.color, cmd.weight, cmd.type))
    }

    @EventSourcingHandler
    fun on(evt: NewPokemonEvent){
        this.id = UUID.randomUUID().toString()
        this.pokemonName = evt.pokemonName
        this.pokemonWeight = evt.weight
        this.type = evt.type
        this.captured = false
    }
}