package com.wbrandt.pokedexDemo.spring.commands

import com.wbrandt.pokedexDemo.spring.api.*
import org.axonframework.commandhandling.CommandHandler
import org.axonframework.eventsourcing.EventSourcingHandler
import org.axonframework.modelling.command.AggregateIdentifier
import org.axonframework.spring.stereotype.Aggregate
import org.slf4j.LoggerFactory
import java.lang.invoke.MethodHandles
import java.util.*

import org.axonframework.modelling.command.AggregateLifecycle.apply
import org.springframework.beans.factory.annotation.Autowired

@Aggregate
class Capture {

    @AggregateIdentifier
    lateinit var pokedexId: String
    lateinit var pokemonId: String
    private val log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass())

    @Autowired
    //private lateinit var repository: PokedexRepository

    constructor() {
        log.debug("empty constructor invoked")
    }

    @CommandHandler
    constructor(cmd: CapturePokemonCommand) {
        log.debug("Handling New Pokemon Command", cmd)
        apply(CapturePokemonEvent(cmd.pokedexId, cmd.pokemonId))
    }

    @EventSourcingHandler
    fun on(evt: CapturePokemonEvent){
        pokedexId = evt.pokedexId
        pokemonId = evt.pokemonId

    }
}