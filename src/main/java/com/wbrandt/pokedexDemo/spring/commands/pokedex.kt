package com.wbrandt.pokedexDemo.spring.commands

import com.wbrandt.pokedexDemo.spring.api.*
import org.axonframework.commandhandling.CommandHandler
import org.axonframework.eventsourcing.EventSourcingHandler
import org.axonframework.modelling.command.AggregateIdentifier
import org.axonframework.spring.stereotype.Aggregate
import org.slf4j.LoggerFactory
import java.lang.invoke.MethodHandles
import java.util.*

import org.axonframework.modelling.command.AggregateLifecycle.apply
import org.axonframework.modelling.command.AggregateVersion

@Aggregate
class Pokedex {

    @AggregateIdentifier
    lateinit var id: String

    @AggregateVersion
    var version: Long = 0

    private var pokedexName: String = ""
    private var pokedexSize: Int = 0

    private val log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass())

    constructor() {
        log.debug("empty constructor invoked")
    }

    @CommandHandler
    constructor(cmd: NewPokedexCommand) {
        log.debug("check the database to see if the id && name are unique")
        apply(NewPokedexEvent(cmd.id, cmd.pokedexName, pokedexSize))
    }

    @CommandHandler
    fun handle(cmd: UpdatePokedexNameCommand){
        log.debug("Update pokedex name command")
        apply(UpdatePokedexNameEvent(cmd.id, cmd.pokedexName))
    }

    @EventSourcingHandler
    fun on(evt: NewPokedexEvent){
        this.id = evt.id
        this.pokedexName = evt.pokedexName
        this.pokedexSize = 0
    }

    @EventSourcingHandler
    fun on(evt: UpdatePokedexSizeEvent){
        pokedexSize = evt.pokedex_size
    }

    @EventSourcingHandler
    fun on (evt: UpdatePokedexNameEvent){
        pokedexName = evt.pokedexName
    }
}