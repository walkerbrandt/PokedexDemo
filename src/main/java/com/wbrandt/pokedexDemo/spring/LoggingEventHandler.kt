package com.wbrandt.pokedexDemo.spring

import org.axonframework.eventhandling.EventHandler
import org.springframework.stereotype.Component

@Component
class LoggingEventHandler {

    @EventHandler
    fun on(event: Any) {
        println("Event received: $event")
    }
}