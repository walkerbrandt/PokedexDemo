package com.wbrandt.pokedexDemo.spring.readside

import com.wbrandt.pokedexDemo.spring.api.NewPokedexEvent
import com.wbrandt.pokedexDemo.spring.api.Pokedex
import com.wbrandt.pokedexDemo.spring.api.UpdatePokedexNameEvent
import org.axonframework.eventhandling.EventHandler
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import javax.persistence.EntityManager

@Component
class PokedexReadside {

    @Autowired
    private lateinit var entityManager: EntityManager

    @EventHandler
    fun on(event: NewPokedexEvent) {
        val pokedex = Pokedex(event.id, event.pokedexName, event.pokedex_size)
        entityManager.persist(pokedex)
    }

    @EventHandler
    fun on(event: UpdatePokedexNameEvent) {
        val pokedex = entityManager.find(Pokedex::class.java, event.id)
        pokedex.pokedexName = event.pokedexName
        entityManager.persist(pokedex)
    }
}
