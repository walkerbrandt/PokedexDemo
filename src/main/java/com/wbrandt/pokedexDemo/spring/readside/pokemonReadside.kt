package com.wbrandt.pokedexDemo.spring.readside

import com.wbrandt.pokedexDemo.spring.api.*
import org.axonframework.eventhandling.EventHandler
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.lang.invoke.MethodHandles

@Component
class PokemonReadside {

    @Autowired
    private lateinit var repository: PokemonRepository
    private val log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass())

    @EventHandler
    fun on(evt: NewPokemonEvent) {
        //val pokedex = repository.findById(evt.id.toLong())
        //log.debug(pokedex.toString())
        repository.save(Pokemon(evt.id, evt.pokemonName, evt.color, evt.weight, evt.type))

    }
}
